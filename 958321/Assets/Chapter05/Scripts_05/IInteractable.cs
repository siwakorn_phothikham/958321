using UnityEngine;

namespace Siwakorn.GameDev3.Chapter5.InteractionSystem
{
    public interface IInteractable
    {
        void Interact(GameObject actor);
    }
}