using Siwakorn.GameDev3.Chapter5.InteractionSystem;

namespace Siwakorn.GameDev3.Chapter9.Interaction
{
    public class ActorTriggerHandlerV2 : ActorTriggerHandler 
    {
        public virtual IInteractable[] GetInteractables()
        {
            //Remove null object from the list
            m_TriggeredGameObjects.RemoveAll(gameject => gameject == null);
            if (m_TriggeredGameObjects.Count == 0)
            {
                return null;
            }
            
            return m_TriggeredGameObjects[0].GetComponents<IInteractable>();
        }
    }
    
    /*public class ActorTriggerHandlerV2 : ActorTriggerHandler
        {
            public virtual IInteractable[] GetInteractables()
            {
                //Remove null object from the list
                m_TriggeredGameObjects.RemoveAll(gameobject => gameobject == null);
               
                if (m_TriggeredGameObjects.Count == 0){
                    return null;
                }
    
                List<IInteractable> interactableList = new();
                foreach (var g in m_TriggeredGameObjects)
                {
                    interactableList.Add(g.GetComponent<IInteractable>());
                }
    
                return interactableList.ToArray();
            }
        }*/
}