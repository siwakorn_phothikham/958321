using System;
using UnityEngine;

namespace Siwakorn.GameDev3.Chapter6.UnityEvents
{
    [RequireComponent(typeof(Rigidbody))]
    public class ObjectPusher : MonoBehaviour
    {
        [SerializeField] private float _forceMagnitude = 10;

        private Rigidbody _rigitbody;

        private void Start()
        {
            _rigitbody = GetComponent<Rigidbody>();
        }

        public void Push(GameObject actor)
        {
            _rigitbody.AddForce(actor.transform.forward * _forceMagnitude, ForceMode.Impulse);
        }
    }
}