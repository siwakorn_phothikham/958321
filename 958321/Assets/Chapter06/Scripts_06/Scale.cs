using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Siwakorn.gameDev3.Chapter6.UnityEvents
{
    public class Scale : MonoBehaviour
    {
        [SerializeField] private float Multiply;

        public void Bigger(GameObject actor)
        {
            Vector3 CurrentScale = actor.transform.localScale;
            Vector3 newscale = CurrentScale * Multiply;
            actor.transform.localScale = newscale;
        }

        public void Small(GameObject actor)
        {
            Vector3 CurrentScale = actor.transform.localScale;
            Vector3 newscale = CurrentScale / Multiply;
            actor.transform.localScale = newscale;
        }
    }
}