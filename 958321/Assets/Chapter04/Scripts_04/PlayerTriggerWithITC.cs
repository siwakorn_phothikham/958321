using System;
using System.Collections;
using System.Collections.Generic; using System.Diagnostics;

using Siwakorn.GameDev3.Chapter1; using UnityEngine;

namespace Siwakorn.GameDev3.Chapter4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            //Get components from item object
            //Get the ItemTypeComponent component from the triggered object
            ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();
            
            //Get components from the player
            //Inventory
            var inventory = GetComponent<Inventory>();
            //SimpleHealthPointComponent
            var simpleHP = GetComponent<SimpleHealthPointComponent>();
            
            if (itc != null)
            {
                switch (itc.Type)
                {
                    case ItemType.COIN:
                        inventory.AddItem("COIN",1); break;
                    
                    case ItemType.BIGCOIN:
                        inventory.AddItem("BIGCOIN",1); break;
                    
                    case ItemType.POWERUP:
                        inventory.AddItem("POWERUP",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10; break;
                    
                    case ItemType.POWERDOWN:
                        inventory.AddItem("POWERDOWN",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10; break;
                    
                    case ItemType.RESTOREHP:
                        inventory.AddItem("RESTOREHP",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 50; break;
                    
                    case ItemType.DAMAGEHP:
                        inventory.AddItem("DAMAGEHP",1);
                        if(simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 5; break;
                }
            }
            
            Destroy(other.gameObject,0);
        }
    }
}