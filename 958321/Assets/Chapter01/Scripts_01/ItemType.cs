namespace Siwakorn.GameDev3.Chapter1
{
    public enum ItemType 
    {
        COIN, 
        BIGCOIN,
        POWERUP,
        POWERDOWN,
        RESTOREHP,
        DAMAGEHP,
    }
}